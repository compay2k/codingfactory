﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fr.CodingFactory.CaddyCrash.Vehicules
{
    public class Bateau : Vehicule
    {
        public Bateau() : base("bleu")
        {

        }

        public override bool EstTerrestre()
        {
            return false;
        }
    }
}
