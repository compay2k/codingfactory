﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoLibrary
{
    public class Personne
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Age { get; set; }
        public int Energie { get; set; }

        public string DireBonjour()
        {
            return "bonjour";
        }

        public int Additionner(int a, int b)
        {
            return a + b;
        }

        public void Manger(string aliment, int qte)
        {
            if (aliment == "sucre")
            {
                qte = qte * 2;
            }

            this.Energie += qte;
        }
    }
}
