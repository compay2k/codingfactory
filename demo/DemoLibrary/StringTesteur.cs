﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoLibrary
{
    public class StringTesteur
    {
        const string valeur = "aeazkkskflksjkfljqlsjflslkwfkqjsfjkhqfhkqshjkhkkqkxjfkqsjhfkjqshjkfhqsjkfkqshfjkqsdqsjhdkjqsjkdhkjqshdkjhqskjdhkjqshdjkqshjkdhqjskdhkjqshdkjhqsjkdhkqjshdkhqsjkdhkjqhsjkdhjkqsd";

        public void TestStandard(int nbPassages)
        {
            string s = "";

            for (int i = 0; i < nbPassages; i++)
            {
                s += valeur;
            }

            Console.WriteLine("Terminé A !");
        }

        public void TestEfficace(int nbPassages)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < nbPassages; i++)
            {
                sb.Append(valeur);
            }

            string s = sb.ToString();

            Console.WriteLine("Terminé B !");
        }
    }
}
