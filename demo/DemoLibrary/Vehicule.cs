﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fr.CodingFactory.CaddyCrash.Vehicules
{
    public abstract class Vehicule
    {
        public Vehicule(string couleur)
        {
            this.Couleur = couleur;
        }

        public string Couleur { get; set; }

        public abstract bool EstTerrestre();

        public void Repeindre(string couleur)
        {
            Couleur = couleur;
        }

        public virtual void Avancer()
        {
            Console.WriteLine("J'avance");
        }
    }
}
