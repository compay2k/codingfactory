﻿using System;

namespace Fr.CodingFactory.CaddyCrash.Vehicules
{
    public class Voiture : Vehicule
    {
        public Voiture(string couleur) : base(couleur)
        {
            
        }

        public int Cylindre { get; set; }
        public int Puissance { get; set; }

        public override void Avancer()
        {
            base.Avancer();
            Console.WriteLine("VROUUUUUMMMM");
        }

        public override bool EstTerrestre()
        {
            return true;
        }
    }
}
