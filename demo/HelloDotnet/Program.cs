﻿using DemoLibrary;
using Fr.CodingFactory.CaddyCrash.Vehicules;
using System;

namespace HelloDotnet
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine("Hello World!");

            Vehicule v = new Voiture("rouge");
            Tester(v);

            Tester(new Bateau());
            */

            StringTesteur t = new StringTesteur();

            DateTime debut = DateTime.Now;
            //t.TestStandard(100000);
            DateTime fin = DateTime.Now;
            TimeSpan duree = fin - debut;
            Console.WriteLine("duree 1 : " + duree.TotalMilliseconds);

            debut = DateTime.Now;
            t.TestEfficace(10000000);
            fin = DateTime.Now;
            duree = fin - debut;
            Console.WriteLine("duree 2 : " + duree.TotalMilliseconds);
            

            Console.ReadLine();
        }

        static void Tester(Vehicule v)
        {
            v.Repeindre("jaune");
            v.Avancer();
            Console.WriteLine(v.Couleur);
        }
    }
}
