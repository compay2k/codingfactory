﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
// MANIP SESSIONS :
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DemoMVC.Controllers
{
    [Route("weather")]
    [Route("meteo")]
    [Route("letemps")]
    public class MeteoController : Controller
    {
        
        public IActionResult Index()
        {
            return View("plop");
        }

        
        [Route("cities")]
        [Route("villes")]
        public IActionResult Villes()
        {
            List<String> mesVilles = new List<string>();

            // indiquer les données à passer à la vue :
            ViewData.Add("Titre", "Villes du monde");
            ViewData.Add("listeVilles", mesVilles);

            mesVilles.Add("Tokyo");
            mesVilles.Add("Sydney");

            //sérialiser la liste des villes :
            string seria = JsonConvert.SerializeObject(mesVilles);
            // stockage de la liste dans la session :
            HttpContext.Session.SetString("listeVilles", seria);

            return View();
        }

        [Route("cities/ajouter/{nom}")]
        public IActionResult AjouterVille(string nom)
        {
            ViewData.Add("Titre", "Villes du monde");

            // je récupère ma liste dans la session (sous forme de string json):
            string deseria = HttpContext.Session.GetString("listeVilles");
            // je désérialise le json pour reconstiture mon objet :
            List<String> mesVilles = JsonConvert.DeserializeObject<List<String>>(deseria);
                       
            // j'ajoute la nouvelle ville à la liste :
            mesVilles.Add(nom);
            
            ViewData.Add("listeVilles", mesVilles);

            // je réécris dans la session :
            string seria = JsonConvert.SerializeObject(mesVilles);
            HttpContext.Session.SetString("listeVilles", seria);


            return View("Villes");
        }


        [Route("voir/{pays=FourbeLand}/{nomVille=Londres}")]
        public string Ville(string pays, string nomVille)
        {
            return "A " + nomVille + " en " + pays + " il neige... pas encore";
        }
    }
}